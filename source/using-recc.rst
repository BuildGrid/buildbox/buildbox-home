.. _recc-client:

recc client
===========

`recc`_ is the **Remote Execution Caching Compiler**, an open source build tool
that wraps compiler command calls and forwards them to a remote build execution
service using the Remote Execution API (REAPI) v2.

.. note::

   There is no stable release of recc yet. You'll have to `install it from
   sources`_.

.. _recc: https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/recc/
.. _install it from sources: https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/README.rst

.. _recc-configuration:

Configuration
-------------

recc reads the configuration from its execution environment. You can get a
complete list of environment variables it accepts by running:

.. code-block:: sh

   recc --help

The variables are prefixed with ``RECC_``. The most important ones for remote
execution are:

- ``RECC_SERVER``: URI of the remote execution server.
- ``RECC_CAS_SERVER``: URI of the CAS server, defaults to ``RECC_SERVER``.
- ``RECC_ACTION_CACHE_SERVER``: URI of the CAS server, defaults to the value of
  ``RECC_CAS_SERVER``.
- ``RECC_INSTANCE``: name of the remote execution instance.

.. hint::

   ``RECC_VERBOSE=1`` can be set in order to enable verbose output.


First recc must be configured to connect to the appropriate endpoints:
to the same remote execution server and CAS server that buildbox-worker is
connected to, plus an Action Cache endpoint.

.. code-block:: sh

  export RECC_SERVER=buildgrid:50051
  export RECC_INSTANCE=main

That points recc to an instance named ``"main"`` on the remote execution
server on ``buildgrid:50051``, and that address will also be used for the CAS
(Content Addressable Storage) and Action Cache services.

If the CAS server and Action Cache services happen to be listening on different
endpoints they must specified individually. For example:

.. code-block:: sh

  export RECC_SERVER=buildgrid:50051
  export RECC_CAS_SERVER=casd:50052
  export RECC_ACTION_CACHE_SERVER=buildgrid:50053
  export RECC_INSTANCE=main

For more details about more advanced options, see recc's README:
https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/recc/README.md

.. _recc-example:

Example build
-------------

recc can be used with any existing software package respecting `GNU make common
variables`_ like ``CC`` for the C compiler or ``CXX`` for the C++ compiler.
We'll focus here on instructions on how to build the `GNU Hello`_ example
program using recc.

First, you need to download the hello source package:

.. code-block:: sh

   wget https://ftp.gnu.org/gnu/hello/hello-2.10.tar.gz

Next, unpack it and change the current directory to the source root:

.. code-block:: sh

   tar xvf hello-2.10.tar.gz
   cd hello-2.10

.. hint::

   All the commands in the instructions below are expected to be executed from
   that root source directory (the GNU Hello project's root directory).

GNU Hello is built using `The Autotools`_ as a build system, so first you'll
need to configure your build by running:

.. code-block:: sh

   ./configure

You can finally build the hello example program, using recc by running:

.. code-block:: sh

   make CC="recc cc"

You can verify that the example program has been successfully built by running
the generated executable. Simply invoke:

.. code-block:: sh

   ./hello


You can also check the logs produced by buildbox-worker to see that it received
the action and launched a runner process to carry out the work.

.. _recc-caching:

Verifying caching
-----------------

You can check the caching functionality by rebuilding the same files.
Clean up your local directory first:

.. code-block:: sh

   make clean

You can then run the build again using recc as before:

.. code-block:: sh

   make CC="recc cc"

This build should hit the Action Cache rather than actually doing any
compilation. That means that buildbox-worker won't receive any work the second
time recc runs.

To obtain more details about the work recc is doing, you can also set
``RECC_VERBOSE=1``.

You can also use the `casdownload`_ command line tool to get the remote
execution server response (an ``ActionResult`` message) from the cache to
manually inspect it to ensure it is as expected.
See the ``casdownload`` README for details on doing that.

.. _GNU make common variables: https://www.gnu.org/software/make/manual/html_node/Implicit-Variables.html
.. _GNU Hello: https://www.gnu.org/software/hello
.. _The Autotools: https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html
.. _casdownload: https://gitlab.com/BuildGrid/buildbox/buildbox/-/tree/master/casdownload
.. _CLI reference section:  https://buildgrid.build/user/reference_cli.html#invoking-bgd-bot-host-tools
.. _Recc's configuration: https://buildgrid.build/user/using_bazel.html#bazel-configuration
.. _BuildGrid: https://buildgrid.build
