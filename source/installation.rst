.. _buildbox-home-installation:

Installation
============

Installing BuildBox requires the user to follow the `buildbox installation guide <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/README.rst>`_ (detailed below).

.. _builboxcommon-dependencies:

Dependencies
----------------------
buildbox relies on:

* gRPC
* Protobuf
* CMake
* GoogleTest
* glog
* pkg-config
* OpenSSL

.. _buildbox-home-gnu-linux:

GNU/Linux
---------

**Debian/Ubuntu**

Install major dependencies along with some other packages through `apt`::

    [sudo] apt-get install cmake gcc g++ googletest libgoogle-glog-dev pkg-config libssl-dev uuid-dev

Install the `googletest`, and `googlemock` binaries ::

    cd /usr/src/googletest && mkdir build && cd build
    cmake .. && [sudo] make install


On Ubuntu, as of 18.04LTS, the package versions of `protobuf` and `grpc` are too old for use with buildbox. Therefore manual build and install is necessary.
The instructions below for installing the `protobuf` compiler and `grpc`  are synthesized from the `grpc source
<https://github.com/grpc/grpc/blob/master/BUILDING.md>`_.

**Last tested with `grpc` version: 1.20.0 and `protoc` version: 3.8.0**

Clone and install the latest release of `grpc` ::

    [sudo] apt-get install build-essential autoconf libtool pkg-config
    git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc
    cd grpc
    git submodule update --init
    [sudo] make install

Check to see if `protoc` is installed by running `protoc --version`, if not found, install the protobuf compiler.

Inside the above `grpc` directory run::

    cd third_party/protobuf
    [sudo] make install

On Debian 10 and newer versions of Ubuntu, you can simply install the packages with `apt`::

    [sudo] apt-get install grpc++ libprotobuf-dev libgmock-dev protobuf-compiler-grpc


.. _buildbox-home-compiling:

Compiling
--------------
Once you've installed the dependencies, you can compile `buildbox` using the following commands in the buildbox directory::

    mkdir build
    cd build
    cmake .. && [sudo] make [install]


Docker images
-------------
Docker images are published on the GitLab registry. For example: ``registry.gitlab.com/buildgrid/buildbox/buildbox:latest``.

In addition, a nightly image containing all the buildbox components can be obtained from ``registry.gitlab.com/buildgrid/buildgrid.hub.docker.com/buildbox:nightly``.
